#! /bin/sh -e

. ./quartus-env.sh

enable_tbbmalloc_workaround

# Required for Qsys to work
quartus_sh --tcl_eval set_user_option -name MANAGED_FLOW_HDL_PREFERENCE VHDL

quartus_sh --flow compile raid
