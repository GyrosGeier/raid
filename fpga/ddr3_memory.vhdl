library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity ddr3_memory is
	generic(
		col_width : positive;
		row_width : positive;
		bank_width : positive;
		word_width : positive
	);
	port(
		a : in std_logic_vector(maximum(13, row_width) - 1 downto 0);
		ba : in std_logic_vector(bank_width - 1 downto 0);
		ck, ck_n : in std_logic;
		cke : in std_logic;
		cs_n : in std_logic;
		dm : in std_logic;
		odt : in std_logic;
		ras_n, cas_n : in std_logic;
		we_n : in std_logic;
		reset_n : in std_logic;

		-- bidirectional signals are split
		dq_i : in std_logic_vector(word_width - 1 downto 0);
		dqs_i, dqs_n_i : in std_logic;
		dq_o : out std_logic_vector(word_width - 1 downto 0);
		dqs_o, dqs_n_o : out std_logic
	);
end entity;

architecture sim of ddr3_memory is
	-- register cke for command decoding
	signal cke_r : std_logic;

	-- mode registers
	signal mr0, mr1, mr2, mr3 : std_logic_vector(12 downto 0);

	-- decoded mode registers
	type mr_burst_length_t is (bl8, otf, bc4, invalid);
	signal mr_burst_length : mr_burst_length_t := invalid;

	type burst_type_t is (sequential, interleaved, invalid);
	signal burst_type : burst_type_t := invalid;

	type cas_latency_t is (reserved, cl5, cl6, cl7, cl8, cl9, cl10, cl11, cl12, cl13, cl14, invalid);
	signal cas_latency : cas_latency_t := invalid;

	type dll_reset_t is (inactive, active, invalid);
	signal dll_reset : dll_reset_t := invalid;

	type write_recovery_t is (wr5, wr6, wr7, wr8, wr10, wr12, wr14, wr16, invalid);
	signal write_recovery : write_recovery_t := invalid;

	type precharge_powerdown_t is (dll_off, dll_on, invalid);
	signal precharge_powerdown : precharge_powerdown_t := invalid;

	type cas_write_latency_t is (cwl5, cwl6, cwl7, cwl8, cwl9, cwl10, reserved, invalid);
	signal cas_write_latency : cas_write_latency_t := invalid;

	-- memory contents will not fit into simulator as a massive array, so allocate on-demand

	-- word size
	subtype word is std_logic_vector(word_width - 1 downto 0);

	-- column definition
	subtype col_data is word;
	constant col_count : natural := 2 ** col_width;
	subtype col is natural range 0 to col_count - 1;
	subtype col_addr is std_logic_vector(col_width - 1 downto 0);
	-- no pointer type here

	-- row definition
	-- always fully populated
	type row_data is array(col) of col_data;
	constant row_count : natural := 2 ** row_width;
	subtype row is natural range 0 to row_count - 1;
	subtype row_addr is std_logic_vector(row_width - 1 downto 0);
	type row_ptr is access row_data;

	-- row list (inside bank)
	-- linked list of rows already accessed
	type row_list_node;
	type row_list_node_ptr is access row_list_node;
	type row_list_node is record
		next_row : row_list_node_ptr;
		address : row;
		data : row_ptr;
	end record;

	-- bank definition
	type bank_data is record
		rows : row_list_node_ptr;
		active_row : row_ptr;
	end record;
	constant bank_count : natural := 2 ** bank_width;
	subtype bank is natural range 0 to bank_count - 1;
	subtype bank_addr is std_logic_vector(bank_width - 1 downto 0);
	type bank_ptr is access bank_data;

	type contents_type is array(bank) of bank_ptr;

	shared variable contents : contents_type := (others => null);

	-- accessor functions
	impure function lookup_bank(constant n : in bank) return bank_ptr is
	begin
		if(contents(n) = null) then
			contents(n) := new bank_data'(null, null);
		end if;
		return contents(n);
	end function;

	impure function lookup_bank(constant ba : in bank_addr) return bank_ptr is
	begin
		return lookup_bank(to_integer(unsigned(ba)));
	end function;

	impure function lookup_row(constant ba : in bank_addr; constant n : in row) return row_ptr is
		variable b : bank_ptr;
		variable p, c, nn : row_list_node_ptr;
		variable d : row_ptr;
	begin
		b := lookup_bank(ba);
		assert b /= null report "bank object is null" severity failure;
		p := b.rows;	-- where the new node should be attached, if necessary
		c := b.rows;	-- current search pointer
		search : loop
			if(c = null) then
				-- search exhausted, create new node
				d := new row_data'(others => (others => 'U'));
				nn := new row_list_node'(null, n, d);
				if(p = null) then
					b.rows := nn;
				else
					p.next_row := nn;
				end if;
				return d;
			elsif(c.address = n) then
				return c.data;
			else
				p := c;
				c := c.next_row;
			end if;
		end loop;
	end function;

	impure function lookup_row(constant ba : in bank_addr; constant ra : in row_addr) return row_ptr is
	begin
		return lookup_row(ba, to_integer(unsigned(ra)));
	end function;

	procedure precharge_bank(constant n : in bank) is
		variable b : bank_ptr;
	begin
		b := lookup_bank(n);
		b.active_row := null;
	end procedure;

	procedure precharge_bank(constant ba : in bank_addr) is
		variable b : bank_ptr;
	begin
		precharge_bank(to_integer(unsigned(ba)));
	end procedure;

	procedure activate_row(constant ba : in bank_addr; constant ra : in row_addr) is
		variable b : bank_ptr;
		variable r : row_ptr;
	begin
		b := lookup_bank(ba);
		assert b.active_row = null report "row activated while another row is active" severity error;
		r := lookup_row(ba, ra);
		b.active_row := r;
	end procedure;

	impure function get_column(constant ba : in bank_addr; constant n : in col) return col_data is
		variable b : bank_ptr;
		variable r : row_ptr;
	begin
		b := lookup_bank(ba);
		r := b.active_row;
		assert r /= null report "read attempted with no row active" severity error;
		return r(n);
	end function;

	impure function get_column(constant ba : in bank_addr; constant ca : in col_addr) return col_data is
	begin
		return get_column(ba, to_integer(unsigned(ca)));
	end function;

	procedure set_column(constant ba : in bank_addr; constant n : in col; constant data : in col_data) is
		variable b : bank_ptr;
		variable r : row_ptr;
	begin
		b := lookup_bank(ba);
		r := b.active_row;
		assert r /= null report "write attempted with no row active" severity error;
		r(n) := data;
		report "write " & to_string(ba) & " " & to_string(n) & " " & to_string(data) severity note;
	end procedure;

	procedure set_column(constant ba : in bank_addr; constant ca : in col_addr; constant data : in col_data) is
	begin
		set_column(ba, to_integer(unsigned(ca)), data);
	end procedure;

	type burst_length is (none, bl8, bl4);

	type pending_access is record
		bl : burst_length;
		ba : bank_addr;
		-- row address is "active row"
		ca : col_addr;
	end record;

	subtype read_latency is natural range 0 to 14;
	subtype write_latency is natural range 0 to 10;

	type pending_reads_t is array(read_latency) of pending_access;
	shared variable pending_reads : pending_reads_t := (others => (none, (others => 'U'), (others => 'U')));

	procedure schedule_read(constant ba : in bank_addr; constant ca : in col_addr; constant bl : in burst_length) is
		variable insert_point : read_latency;
	begin
		case cas_latency is
			when cl5 => insert_point := 5;
			when cl6 => insert_point := 6;
			when cl7 => insert_point := 7;
			when cl8 => insert_point := 8;
			when cl9 => insert_point := 9;
			when cl10 => insert_point := 10;
			when cl11 => insert_point := 11;
			when cl12 => insert_point := 12;
			when cl13 => insert_point := 13;
			when cl14 => insert_point := 14;
			when reserved => report "reserved CAS write latency used" severity error;
			when invalid => report "CAS write latency not configured correctly" severity error;
		end case;

		pending_reads(insert_point) := (bl, ba, ca);
	end procedure;

	type pending_writes_t is array(write_latency) of pending_access;
	shared variable pending_writes : pending_writes_t := (others => (none, (others => 'U'), (others => 'U')));

	procedure schedule_write(constant ba : in bank_addr; constant ca : in col_addr; constant bl : in burst_length) is
		variable insert_point : write_latency;
	begin
		case cas_write_latency is
			when cwl5 => insert_point := 5;
			when cwl6 => insert_point := 6;
			when cwl7 => insert_point := 7;
			when cwl8 => insert_point := 8;
			when cwl9 => insert_point := 9;
			when cwl10 => insert_point := 10;
			when reserved => report "reserved CAS write latency used" severity error;
			when invalid => report "CAS write latency not configured correctly" severity error;
		end case;

		pending_writes(insert_point) := (bl, ba, ca);
	end procedure;

	-- TODO: remove
	signal test : std_logic;
begin
	process(ck, reset_n)
		variable command : std_logic_vector(9 downto 0);
	begin
		if(reset_n = '0') then
			-- TODO: check that cke is zero when reset goes inactive
			cke_r <= '0';
		elsif(rising_edge(ck)) then
			command := cke_r & cke & cs_n & ras_n & cas_n & we_n & a(12) & a(10) & mr0(1 downto 0);
			case? command is
				when "00--------" =>
					-- TODO: clock is disabled
					null;

				when "110000----" =>
					case ba is
						when "000" =>
							mr0 <= a(12 downto 0);
						when "001" =>
							mr1 <= a(12 downto 0);
						when "010" =>
							mr2 <= a(12 downto 0);
						when "011" =>
							mr3 <= a(12 downto 0);
						when others =>
							report "invalid mode register set" severity error;
					end case;

				when "110001----" =>
					report "refresh" severity note;
				when "100001----" =>
					report "self refresh entry" severity note;
				when "011-------"
				   | "010111----" =>
					report "self refresh exit" severity note;
				when "110010-0--" =>
					report "precharge" severity note;
					precharge_bank(
						ba(bank_addr'range));
				when "110010-1--" =>
					report "precharge all banks" severity note;
					for i in bank loop
						precharge_bank(i);
					end loop;
				when "110011----" =>
					report "activate" severity note;
					activate_row(
						ba(bank_addr'range),
						a(row_addr'range));
				when "110100-000"
				   | "1101001001" =>
					report "write long burst (8)" severity note;
					schedule_write(ba(bank_addr'range), a(col_addr'range), bl8);
				when "110100-010"
				   | "1101000001" =>
					report "write chopped burst (4)" severity note;
					schedule_write(ba(bank_addr'range), a(col_addr'range), bl4);
				when "110100-100"
				   | "1101001101" =>
					report "write long burst (8) with auto precharge" severity note;
					schedule_write(ba(bank_addr'range), a(col_addr'range), bl8);
				when "110100-110"
				   | "1101000101" =>
					report "write chopped burst (4) with auto precharge" severity note;
					schedule_write(ba(bank_addr'range), a(col_addr'range), bl4);
				when "110101-000"
				   | "1101011001" =>
					report "read long burst (8)" severity note;
					schedule_read(ba(bank_addr'range), a(col_addr'range), bl8);
				when "110101-010"
				   | "1101010001" =>
					report "read chopped burst (4)" severity note;
					schedule_read(ba(bank_addr'range), a(col_addr'range), bl4);
				when "110101-100"
				   | "1101011101" =>
					report "read long burst (8) with auto precharge" severity note;
					schedule_read(ba(bank_addr'range), a(col_addr'range), bl8);
				when "110101-110"
				   | "1101010101" =>
					report "read chopped burst (4) with auto precharge" severity note;
					schedule_read(ba(bank_addr'range), a(col_addr'range), bl4);
				when "110111----" =>
					-- no operation
					-- TODO: clock is enabled here
				when "111-------" =>
					-- device deselected
					-- TODO: clock is enabled here
				when "100111----"
				   | "101-------" =>
					report "power-down entry" severity note;
				-- TODO: conflict with self-refresh exit
				--when "010111----" =>
				--   | "011-------" =>
				--	report "power-down exit" severity note;
				when "110110-1--" =>
					report "zq calibration long" severity note;
				when "110110-0--" =>
					report "zq calibration short" severity note;
				when others =>
					report "invalid command" severity error;
			end case?;

			cke_r <= cke;
		end if;
	end process;

	-- handle reads
	process(reset_n, ck)
		variable ba : bank_addr;
		variable ca : col_addr;
		subtype burst_remaining is natural range 0 to 8;
		variable br : burst_remaining := 0;
	begin
		if(reset_n = '0') then
			dq_o <= (others => 'Z');
			dqs_o <= 'Z';
			dqs_n_o <= 'Z';
		elsif(ck'event) then
			-- start burst on rising DQS edge
			if(ck = '1' and pending_reads(0).bl /= none) then
				assert br = 0 report "Read burst started while another in progress" severity error;
				ba := pending_reads(0).ba;
				ca := pending_reads(0).ca;
				case pending_reads(0).bl is
					when none => null;		-- cannot happen
					when bl8 => br := 8;
					when bl4 => br := 4;
				end case;
			end if;
			-- toggle clock if inside or immediately before a burst
			if(br /= 0 or pending_reads(1).bl /= none) then
				dqs_o <= ck after 50 ps;
				dqs_n_o <= ck_n after 50 ps;
			else
				dqs_o <= 'Z' after 50 ps;
				dqs_n_o <= 'Z' after 50 ps;
			end if;
			if(br /= 0) then
				dq_o <= get_column(
					ba,
					ca);
				-- TODO: interleaving mode?
				ca := std_logic_vector(unsigned(ca) + 1);
				br := br - 1;
			else
				dq_o <= (others => 'Z');
			end if;
			if(ck = '0') then
				pending_reads(read_latency'low to read_latency'high - 1) :=
					pending_reads(read_latency'low + 1 to read_latency'high);
				pending_reads(read_latency'high) := (none, (others => 'U'), (others => 'U'));
			end if;
		end if;
	end process;

	-- handle writes
	process(reset_n, ck, dqs_i)
		variable ba : bank_addr;
		variable ca : col_addr;
		subtype burst_remaining is natural range 0 to 8;
		variable br : burst_remaining := 0;
	begin
		if(reset_n = '0') then
			null;
		elsif(dqs_i'event) then
			-- start burst on rising DQS edge
			if(dqs_i = '1' and pending_writes(0).bl /= none) then
				assert br = 0 report "Write burst started while another in progress" severity error;
				ba := pending_writes(0).ba;
				ca := pending_writes(0).ca;
				case pending_writes(0).bl is
					when none => null;		-- cannot happen
					when bl8 => br := 8;
					when bl4 => br := 4;
				end case;
			end if;
			if(br /= 0) then
				set_column(
					ba,
					ca,
					dq_i(col_data'range));
				-- TODO: interleaving mode?
				ca := std_logic_vector(unsigned(ca) + 1);
				br := br - 1;
			end if;
		end if;
		if(reset_n = '0') then
			null;
		elsif(ck = '0') then
			pending_writes(write_latency'low to write_latency'high - 1) :=
				pending_writes(write_latency'low + 1 to write_latency'high);
			pending_writes(write_latency'high) := (none, (others => 'U'), (others => 'U'));
		end if;
	end process;

	-- report mode register changes
	process(mr0)
	begin
		report "mode register 0 set " & to_string(mr0'last_value) & " -> " & to_string(mr0) severity note;
	end process;

	process(mr1)
	begin
		report "mode register 1 set " & to_string(mr1'last_value) & " -> " & to_string(mr1) severity note;
	end process;

	process(mr2)
	begin
		report "mode register 2 set " & to_string(mr2'last_value) & " -> " & to_string(mr2) severity note;
	end process;

	process(mr3)
	begin
		report "mode register 3 set " & to_string(mr3'last_value) & " -> " & to_string(mr3) severity note;
	end process;

	-- decode mode registers
	with mr0(1 downto 0) select mr_burst_length <=
		bl8 when "00",
		otf when "01",
		bc4 when "10",
		invalid when others;

	process(mr_burst_length)
	begin
		report "  burst length " & to_string(mr_burst_length'last_value) & " -> " & to_string(mr_burst_length) severity note;
	end process;

	with mr0(3) select burst_type <=
		sequential when '0',
		interleaved when '1',
		invalid when others;

	process(burst_type)
	begin
		report "  burst type " & to_string(burst_type'last_value) & " -> " & to_string(burst_type) severity note;
	end process;

	with mr0(6 downto 4) & mr0(2) select cas_latency <=
		reserved when "0000",
		cl5 when "0010",
		cl6 when "0100",
		cl7 when "0110",
		cl8 when "1000",
		cl9 when "1010",
		cl10 when "1100",
		cl11 when "1110",
		cl12 when "0001",
		cl13 when "0011",
		cl14 when "0101",
		invalid when others;

	process(cas_latency)
	begin
		report "  CAS latency " & to_string(cas_latency'last_value) & " -> " & to_string(cas_latency) severity note;
	end process;

	with mr0(8) select dll_reset <=
		inactive when '0',
		active when '1',
		invalid when others;

	process(dll_reset)
	begin
		report "  DLL reset " & to_string(dll_reset'last_value) & " -> " & to_string(dll_reset) severity note;
	end process;

	with mr0(11 downto 9) select write_recovery <=
		wr16 when "000",
		wr5 when "001",
		wr6 when "010",
		wr7 when "011",
		wr8 when "100",
		wr10 when "101",
		wr12 when "110",
		wr14 when "111",
		invalid when others;

	process(write_recovery)
	begin
		report "  write recovery " & to_string(write_recovery'last_value) & " -> " & to_string(write_recovery) severity note;
	end process;

	with mr0(12) select precharge_powerdown <=
		dll_off when '0',
		dll_on when '1',
		invalid when others;

	process(precharge_powerdown)
	begin
		report "  precharge powerdown " & to_string(precharge_powerdown'last_value) & " -> " & to_string(precharge_powerdown) severity note;
	end process;

	with mr2(5 downto 3) select cas_write_latency <=
		cwl5 when "000",
		cwl6 when "001",
		cwl7 when "010",
		cwl8 when "011",
		cwl9 when "100",
		cwl10 when "101",
		reserved when "110" | "111",
		invalid when others;

	process(cas_write_latency)
	begin
		report "  CAS write latency " & to_string(cas_write_latency'last_value) & " -> " & to_string(cas_write_latency) severity note;
	end process;
end architecture;
