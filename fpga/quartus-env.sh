#! /bin/false
# This should be sourced, not run separately

QUARTUS_ROOT=/opt/altera/20.1
PATH=${QUARTUS_ROOT}/quartus/bin:${QUARTUS_ROOT}/modelsim_ase/bin:${QUARTUS_ROOT}/quartus/sopc_builder/bin/:$PATH

enable_tbbmalloc_workaround() {
	if [ -f /opt/lib/i386-linux-gnu/hack.so ]; then
		LD_PRELOAD='/opt/${LIB}/hack.so'
		export LD_PRELOAD
	fi
}

disable_tbbmalloc_workaround() {
	unset LD_PRELOAD
}
