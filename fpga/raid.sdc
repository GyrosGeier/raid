create_clock -name sata_refclk_0 -period 2ns [get_ports {sata_refclk[0]}]
create_clock -name sata_refclk_1 -period 2ns [get_ports {sata_refclk[1]}]

create_clock -name mem32_refclk -period 2ns [get_ports {mem32_refclk}]
create_clock -name mem16_refclk -period 2ns [get_ports {mem16_refclk}]

create_clock -name eth_rx_clk_0 -period 8ns [get_ports {eth_rx_clk_0}]
create_clock -name eth_rx_clk_1 -period 8ns [get_ports {eth_rx_clk_1}]
create_clock -name eth_rx_clk_2 -period 8ns [get_ports {eth_rx_clk_2}]
create_clock -name eth_rx_clk_3 -period 8ns [get_ports {eth_rx_clk_3}]

derive_pll_clocks
derive_clock_uncertainty
