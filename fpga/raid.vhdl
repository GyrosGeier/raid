library IEEE;
use ieee.std_logic_1164.ALL;
library ddr3_bottom;
use ddr3_bottom.ddr3_bottom;
--library ethernet;
--use ethernet.ethernet;

--library everything;
--use everything.everything;

entity raid is
	port(
		nreset : in std_logic;

		sata_refclk : in std_logic_vector(1 downto 0);
		sata_rx : in std_logic_vector(5 downto 0);
		sata_tx : out std_logic_vector(5 downto 0);

		mem32_refclk : in std_logic;
		mem32_a                      : out   std_logic_vector(15 downto 0);
		mem32_ba                     : out   std_logic_vector(2 downto 0);
		mem32_ck                     : out   std_logic_vector(0 downto 0);
		mem32_ck_n                   : out   std_logic_vector(0 downto 0);
		mem32_cke                    : out   std_logic_vector(1 downto 0);
		mem32_cs_n                   : out   std_logic_vector(1 downto 0);
		mem32_dm                     : out   std_logic_vector(4 downto 0);
		mem32_ras_n                  : out   std_logic_vector(0 downto 0);
		mem32_cas_n                  : out   std_logic_vector(0 downto 0);
		mem32_we_n                   : out   std_logic_vector(0 downto 0);
		mem32_reset_n                : out   std_logic;
		mem32_dq                     : inout std_logic_vector(39 downto 0);
		mem32_dqs                    : inout std_logic_vector(4 downto 0);
		mem32_dqs_n                  : inout std_logic_vector(4 downto 0);
		mem32_odt                    : out   std_logic_vector(1 downto 0);

		mem32_oct_rzqin              : in    std_logic;

		eth_tx_clk_0          : out  std_logic;
		eth_rx_clk_0          : in  std_logic;
		eth_rgmii_in_0        : in  std_logic_vector(3 downto 0);
		eth_rgmii_out_0       : out std_logic_vector(3 downto 0);
		eth_rx_control_0      : in  std_logic;
		eth_tx_control_0      : out std_logic;

		eth_mdc : out std_logic;
		eth_mdio : inout std_logic
	);
end entity;

architecture syn of raid is
	attribute period : time;
	
	attribute period of sata_refclk : signal is 2 ns;

	signal reset_long, nreset_long : std_logic;
	
	constant sata_num_channels : natural := 6;
	constant sata_data_width : natural := 32;

	subtype sata_channel is natural range sata_num_channels - 1 downto 0;

	subtype std_logic_per_sata_channel is
		std_logic_vector(sata_channel);

	type data_per_sata_channel is
		array(sata_channel) of
			std_logic_vector(sata_data_width - 1 downto 0);

	signal sata_rx_pclk : std_logic_per_sata_channel;
	signal sata_rx_ready : std_logic_per_sata_channel;
	signal sata_rx_valid : std_logic_per_sata_channel;
	signal sata_rx_data : data_per_sata_channel;
	signal sata_rx_sop : std_logic_per_sata_channel;
	signal sata_rx_eop : std_logic_per_sata_channel;
	signal sata_rx_err : std_logic_per_sata_channel;

	signal sata_tx_pclk : std_logic_per_sata_channel;
	signal sata_tx_ready : std_logic_per_sata_channel;
	signal sata_tx_valid : std_logic_per_sata_channel;
	signal sata_tx_data : data_per_sata_channel;
	signal sata_tx_sop : std_logic_per_sata_channel;
	signal sata_tx_eop : std_logic_per_sata_channel;
	signal sata_tx_err : std_logic_per_sata_channel;

	type sata_pdata is
		array(sata_channel) of
			std_logic_vector(15 downto 0);

	signal app_clk : std_logic;

	-- these are synchronous to app_clk
	signal sata_rx_pdata : sata_pdata;
	signal sata_tx_pdata : sata_pdata;

	signal eth_refclk : std_logic;

	signal eth_speed_setting : std_logic_vector(7 downto 0);

	signal eth_mdio_in : std_logic;
	signal eth_mdio_out : std_logic;
	signal eth_mdio_oe : std_logic;
begin
	-- minimum reset length
	gen_reset : entity work.reset_extender
		generic map(
			clk_period => sata_refclk'period,
			hold_time => 20 ns
		)
		port map(
			clk => sata_refclk(0),
			input => not nreset,
			output => reset_long
		);

	nreset_long <= not reset_long;

	-- todo use a PLL
	eth_refclk <= '0' when nreset = '0' else not eth_refclk when rising_edge(sata_refclk(0));

	app_clk <= '0' when nreset = '0' else not app_clk when rising_edge(eth_refclk);

	sata : entity work.sata_ports_cyclonev
		generic map(
			num_channels => 6,
			num_triplets => 2
		)
		port map(
			-- reset
			nreset => nreset_long,

			-- generic clock (125 MHz)
			clk => app_clk,

			-- serial interface
			ser_refclk => sata_refclk,
			ser_rx => sata_rx,
			ser_tx => sata_tx,

			-- parallel interface
			rx_clk => open,
			rx_data => open,

			tx_clk => open,
			tx => (others => (("00000000", '0', '0'), ("00000000", '0', '0'))),

			-- OOB signalling
			rx_signaldetect => open,
			tx_elecidle => (others => '0'),

			-- reconfiguration interface (Avalon-MM)
			mgmt_clk_clk => app_clk,
			mgmt_rst_reset => not nreset_long,

			reconfig_mgmt_address => (others => '0'),
			reconfig_mgmt_read => '0',
			reconfig_mgmt_readdata => open,
			reconfig_mgmt_waitrequest => open,
			reconfig_mgmt_write => '0',
			reconfig_mgmt_writedata => (others => '0')
		);

	cache : entity ddr3_bottom.ddr3_bottom
		port map(
			pll_ref_clk => mem32_refclk,

			global_reset_n => nreset,
			soft_reset_n => nreset,

			mem_a => mem32_a,
			mem_ba => mem32_ba,
			mem_ck => mem32_ck,
			mem_ck_n => mem32_ck_n,
			mem_cke => mem32_cke,
			mem_cs_n => mem32_cs_n,
			mem_dm => mem32_dm,
			mem_ras_n => mem32_ras_n,
			mem_cas_n => mem32_cas_n,
			mem_we_n => mem32_we_n,
			mem_reset_n => mem32_reset_n,
			mem_dq => mem32_dq,
			mem_dqs => mem32_dqs,
			mem_dqs_n => mem32_dqs_n,
			mem_odt => mem32_odt,

			oct_rzqin => mem32_oct_rzqin,

			afi_clk => open,
			afi_half_clk => open,
			afi_reset_n => open,
			afi_reset_export_n => open,

			avl_ready_0 => open,
			avl_burstbegin_0 => '0',
			avl_addr_0 => (others => '0'),
			avl_rdata_valid_0 => open,
			avl_rdata_0 => open,
			avl_wdata_0 => (others => '0'),
			avl_be_0 => (others => '0'),
			avl_read_req_0 => '0',
			avl_write_req_0 => '0',
			avl_size_0 => (others => '0'),

			mp_cmd_clk_0_clk => app_clk,
			mp_cmd_reset_n_0_reset_n => nreset,
			mp_rfifo_clk_0_clk => app_clk,
			mp_rfifo_reset_n_0_reset_n => nreset,
			mp_wfifo_clk_0_clk => app_clk,
			mp_wfifo_reset_n_0_reset_n => nreset,

			local_init_done => open,
			local_cal_success => open,
			local_cal_fail => open,

			pll_mem_clk => open,
			pll_write_clk => open,
			pll_locked => open,
			pll_write_clk_pre_phy_clk => open,
			pll_addr_cmd_clk => open,
			pll_avl_clk => open,
			pll_config_clk => open,
			pll_mem_phy_clk => open,
			afi_phy_clk => open,
			pll_avl_phy_clk => open,

			csr_clk => app_clk,
			csr_reset_n => nreset,
			csr_addr => (others => '0'),
			csr_read_req => '0',
			csr_rdata => open,
			csr_write_req => '0',
			csr_wdata => (others => '0'),
			csr_waitrequest => open,
			csr_be => (others => '0'),
			csr_rdata_valid => open
		);

	network_mdio : entity work.bidir
		port map(
			dataio(0) => eth_mdio,
			datain(0) => eth_mdio_out,
			dataout(0) => eth_mdio_in,
			oe(0)	=> eth_mdio_oe
		);

--	eth_tx_clk_0 <= eth_refclk(0);
--
--	network : entity ethernet.ethernet
--		port map(
--			mdc => eth_mdc,
--			mdio_in => eth_mdio_in,
--			mdio_out => eth_mdio_out,
--			mdio_oen => eth_mdio_oe,
--
--			tx_clk_0	=> eth_refclk(0),
--			rx_clk_0 => eth_rx_clk_0,
--			rgmii_in_0 => eth_rgmii_in_0,
--			rgmii_out_0 => eth_rgmii_out_0,
--			rx_control_0 => eth_rx_control_0,
--			tx_control_0 => eth_tx_control_0,
--
--			tx_clk_1	=> eth_refclk(0),
--			rx_clk_1 => eth_rx_clk_1,
--			rgmii_in_1 => eth_rgmii_in_1,
--			rgmii_out_1 => eth_rgmii_out_1,
--			rx_control_1 => eth_rx_control_1,
--			tx_control_1 => eth_tx_control_1,
--
--			tx_clk_2	=> eth_refclk(0),
--			rx_clk_2 => eth_rx_clk_2,
--			rgmii_in_2 => eth_rgmii_in_2,
--			rgmii_out_2 => eth_rgmii_out_2,
--			rx_control_2 => eth_rx_control_2,
--			tx_control_2 => eth_tx_control_2,
--
--			tx_clk_3	=> eth_refclk(0),
--			rx_clk_3 => eth_rx_clk_3,
--			rgmii_in_3 => eth_rgmii_in_3,
--			rgmii_out_3 => eth_rgmii_out_3,
--			rx_control_3 => eth_rx_control_3,
--			tx_control_3 => eth_tx_control_3
--
--		);
end architecture;
