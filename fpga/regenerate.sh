#! /bin/sh -x

set -e

if [ "$1" = "-r" ]; then
	if [ -z "$DISPLAY" ]; then
		if [ -z "`which xvfb-run`" ]; then
			echo >&2 "MegaWizard requires an X server."
			echo >&2
			echo >&2 "Either run from a graphical session or install xvfb"
			exit 1
		fi
		# reexec self under xvfb
		exec xvfb-run $0 "$*"
	fi
fi

. ./quartus-env.sh

if [ "$1" = "-r" ]; then
	enable_tbbmalloc_workaround

	qmegawiz -silent bidir.vhd
	qmegawiz -silent ddr3_bottom.vhd
	qmegawiz -silent sata_phy.vhd
	qmegawiz -silent sata_phy_reconfig.vhd
	qmegawiz -silent sata_tx_pll.vhd

	# fix up DDR3 simulation files
	sed -i -e '/^begin/a csr_reset_n_ports_inv <= not csr_reset_n;' ddr3_bottom_sim/ddr3_bottom/ddr3_bottom_0002.vhd

	disable_tbbmalloc_workaround
fi

# fix up library names
for i in ddr3_bottom sata_phy sata_phy_reconfig sata_tx_pll; do
	xmlstarlet ed --inplace --insert '/simPackage/file[not(@library)]' --type attr -n library -v $i $i.spd
done

rm -rf simulation
ip-setup-simulation --output-directory=simulation --quartus-project=raid

cd simulation

# fix up after rebranding
sed -i -e '/ALTERA/,/}/d' mentor/msim_setup.tcl

# set up "work" library
mkdir -p libraries
vlib libraries/work
vmap work libraries/work

# compile IP blocks
vsim -batch <<EOF
source mentor/msim_setup.tcl
com
EOF

# compile simple IP blocks (not namespaced, single file)
vcom -2008 ../bidir.vhd
