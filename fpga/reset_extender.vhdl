library IEEE;
use IEEE.std_logic_1164.ALL;

entity reset_extender is
	generic(
		clk_period : time;
		hold_time : time
	);
	port(
		clk : in std_logic;
		input : in std_logic;
		output : out std_logic
	);
end entity;

architecture rtl of reset_extender is
	subtype counter_type is integer range hold_time / clk_period downto -1;
	signal counter : counter_type;
begin
	counter <= counter_type'high when input = '1' else
		   unaffected when counter < 0 else
		   counter - 1 when rising_edge(clk);
	output <= '0' when counter < 0 else
		  '1';
end architecture;
