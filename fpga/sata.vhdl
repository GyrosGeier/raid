library IEEE;
use IEEE.std_logic_1164.ALL;

package sata is
	constant gen1_speed_bitpersecond : natural := 1_500_000_000;

	type gen1_ui is range 0 to 2147483647
		units
			ui;
		end units;

	function to_time(constant i : in gen1_ui) return time;

	-- link layer types
	type ll_byte is record
		data : std_logic_vector(7 downto 0);
		datak : std_logic;
		valid : std_logic;
	end record;

	-- two-byte link layer interface
	type ll_data16 is array(1 downto 0) of ll_byte;
	type ll_data16_vector is array(natural range <>) of ll_data16;

end package;

package body sata is
	function to_time(constant i : in gen1_ui) return time is
	begin
		return i / 1 ui * 1 sec / gen1_speed_bitpersecond;
	end function;
end package body;
