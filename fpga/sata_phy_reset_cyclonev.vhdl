library IEEE;
use IEEE.std_logic_1164.ALL;

entity sata_phy_reset_cyclonev is
	generic(
		num_rx_channels : natural;
		num_tx_plls : natural;
		num_tx_channels : natural
	);
	port(
		reset : in std_logic;			-- active high
		clk : in std_logic;			-- 125 MHz

		rx_phy_cal_busy : in std_logic_vector(num_rx_channels - 1 downto 0);
		rx_analogreset : out std_logic_vector(num_rx_channels - 1 downto 0);
		rx_digitalreset : out std_logic_vector(num_rx_channels - 1 downto 0);

		rx_set_locktoref : out std_logic_vector(num_rx_channels - 1 downto 0);
		rx_set_locktodata : out std_logic_vector(num_rx_channels - 1 downto 0);
		rx_std_signaldetect : in std_logic_vector(num_rx_channels - 1 downto 0);
		rx_is_lockedtoref : in std_logic_vector(num_rx_channels - 1 downto 0);
		rx_is_lockedtodata : in std_logic_vector(num_rx_channels - 1 downto 0);

		tx_pll_cal_busy : in std_logic_vector(num_tx_plls - 1 downto 0);
		tx_pll_powerdown : out std_logic_vector(num_tx_plls - 1 downto 0);
		tx_pll_locked : in std_logic_vector(num_tx_plls - 1 downto 0);

		tx_phy_cal_busy : in std_logic_vector(num_tx_channels - 1 downto 0);
		tx_analogreset : out std_logic_vector(num_tx_channels - 1 downto 0);
		tx_digitalreset : out std_logic_vector(num_tx_channels - 1 downto 0);

		rx_ready : out std_logic_vector(num_rx_channels - 1 downto 0);
		tx_ready : out std_logic_vector(num_tx_channels - 1 downto 0)
	);
end entity;

architecture rtl of sata_phy_reset_cyclonev is
	attribute period : time;
	attribute period of clk : signal is 8 ns;

	-- time constants
	constant rx_analogreset_time : time := 40 ns;
	constant rx_ltr_ltd_time : time := 15 us;
	constant rx_digitalreset_time : time := 4 us;
	constant rx_ready_time : time := 20 ns;

	constant tx_pll_reset_time : time := 2 us;
	constant tx_analogreset_time : time := 0 ns;	-- follows pll_powerdown, no extra time
	constant tx_digitalreset_time : time := 20 ns;
	constant tx_ready_time : time := 20 ns;

	subtype rx_channel is integer range num_rx_channels - 1 downto 0;
	subtype tx_pll is integer range num_tx_plls - 1 downto 0;
	subtype tx_channel is integer range num_tx_channels - 1 downto 0;

	function channel_to_pll(ch : in tx_channel) return tx_pll is
	begin
		return (ch / 3);	-- 0,1,2 -> 0, 3,4,5 -> 1
	end function;

	signal tx_pll_powerdown_int : std_logic_vector(tx_pll);

	signal tx_analogreset_int : std_logic_vector(tx_channel);
	signal tx_digitalreset_int : std_logic_vector(tx_channel);

	signal rx_analogreset_int : std_logic_vector(rx_channel);
	signal rx_nset_locktodata_int : std_logic_vector(rx_channel);
	signal rx_digitalreset_int : std_logic_vector(rx_channel);

	signal tx_nready : std_logic_vector(tx_channel);
	signal rx_nready : std_logic_vector(rx_channel);
begin
	tx_pll_reset : for i in tx_pll generate 
	begin
		gen_reset : entity work.reset_extender
			generic map(
				clk_period => clk'period,
				hold_time => tx_pll_reset_time
			)
			port map(
				clk => clk,
				input => reset or tx_pll_cal_busy(i),		-- todo: check
				output => tx_pll_powerdown_int(i)
			);

		tx_pll_powerdown(i) <= tx_pll_powerdown_int(i);
	end generate;

	tx_channel_reset : for i in tx_channel generate
		signal tx_pll_or_phy_cal_busy : std_logic;
	begin
		tx_pll_or_phy_cal_busy <= tx_pll_cal_busy(channel_to_pll(i)) or tx_phy_cal_busy(i);

		gen_tx_analogreset : entity work.reset_extender
			generic map(
				clk_period => clk'period,
				hold_time => tx_analogreset_time
			)
			port map(
				clk => clk,
				input => tx_pll_powerdown_int(channel_to_pll(i)),
				output => tx_analogreset_int(i)
			);
		tx_analogreset(i) <= tx_analogreset_int(i);

		gen_tx_digitalreset : entity work.reset_extender
			generic map(
				clk_period => clk'period,
				hold_time => tx_digitalreset_time
			)
			port map(
				clk => clk,
				input => tx_analogreset_int(i) or tx_phy_cal_busy(i),
				output => tx_digitalreset_int(i)
			);
		tx_digitalreset(i) <= tx_digitalreset_int(i);

		gen_tx_ready : entity work.reset_extender
			generic map(
				clk_period => clk'period,
				hold_time => tx_ready_time
			)
			port map(
				clk => clk,
				input => tx_digitalreset_int(i),
				output => tx_nready(i)
			);
	end generate;

	rx_channel_reset : for i in rx_channel generate
	begin
		-- analog reset needs to be pulled at startup or when the PHY is reconfigured
		gen_rx_analogreset : entity work.reset_extender
			generic map(
				clk_period => clk'period,
				hold_time => rx_analogreset_time
			)
			port map(
				clk => clk,
				input => reset or rx_phy_cal_busy(i),
				output => rx_analogreset_int(i)
			);
		rx_analogreset(i) <= rx_analogreset_int(i);

		-- always set (manual mode), becomes a Don't Care when locktodata is set
		rx_set_locktoref(i) <= '1';

		-- we lock to data when we have been locked to reference for t_LTRLTD with
		-- no glitches
		gen_rx_set_locktodata : entity work.reset_extender
			generic map(
				clk_period => clk'period,
				hold_time => rx_ltr_ltd_time
			)
			port map(
				clk => clk,
				input => rx_analogreset_int(i) or		-- after reset, or
					not rx_std_signaldetect(i) or		-- CDR can't lock on, or
					(
						not rx_is_lockedtodata(i) and	-- lockedtoref is valid and
						not rx_is_lockedtoref(i)),	-- not locked to ref
				output => rx_nset_locktodata_int(i)
			);

		rx_set_locktodata(i) <= not rx_nset_locktodata_int(i);

		gen_rx_digitalreset : entity work.reset_extender
			generic map(
				clk_period => clk'period,
				hold_time => rx_digitalreset_time
			)
			port map(
				clk => clk,
				input => rx_analogreset_int(i) or not rx_is_lockedtodata(i),
				output => rx_digitalreset_int(i)
			);
		rx_digitalreset(i) <= rx_digitalreset_int(i);

		gen_rx_ready : entity work.reset_extender
			generic map(
				clk_period => clk'period,
				hold_time => rx_ready_time
			)
			port map(
				clk => clk,
				input => rx_digitalreset_int(i),
				output => rx_nready(i)
			);
	end generate;

	tx_ready <= not tx_nready;
	rx_ready <= not rx_nready;
end architecture;
