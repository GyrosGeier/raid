library IEEE;
use IEEE.std_logic_1164.ALL;

library sata_tx_pll;
use sata_tx_pll.sata_tx_pll;

library sata_phy;
use sata_phy.sata_phy;

library sata_phy_reconfig;
use sata_phy_reconfig.sata_phy_reconfig;

use work.sata;

entity sata_ports_cyclonev is
	generic(
		num_channels : natural;
		num_triplets : natural
	);
	port(
		-- reset
		nreset : in std_logic;

		-- generic clock (125 MHz)
		clk : in std_logic;

		-- serial interface
		ser_refclk : in std_logic_vector(num_triplets - 1 downto 0);
		ser_rx : in std_logic_vector(num_channels - 1 downto 0);
		ser_tx : out std_logic_vector(num_channels - 1 downto 0);

		-- parallel interface
		rx_clk : out std_logic_vector(num_channels - 1 downto 0);
		rx_data : out sata.ll_data16_vector(num_channels - 1 downto 0);

		tx_clk : out std_logic_vector(num_channels - 1 downto 0);
		tx : in sata.ll_data16_vector(num_channels - 1 downto 0);

		-- OOB signalling
		rx_signaldetect : out std_logic_vector(num_channels - 1 downto 0);
		tx_elecidle : in std_logic_vector(num_channels - 1 downto 0);

		-- reconfiguration interface (Avalon-MM)
		mgmt_clk_clk : in std_logic;
		mgmt_rst_reset : in std_logic;

		reconfig_mgmt_address : in std_logic_vector(6 downto 0);
		reconfig_mgmt_read : in std_logic;
		reconfig_mgmt_readdata : out std_logic_vector(31 downto 0);
		reconfig_mgmt_waitrequest : out std_logic;
		reconfig_mgmt_write : in std_logic;
		reconfig_mgmt_writedata : in std_logic_vector(31 downto 0)
	);
end entity;

architecture rtl of sata_ports_cyclonev is
	attribute period : time;
	attribute period of clk : signal is 8 ns;

	constant channels_per_triplet : natural := 3;

	subtype channel is natural range num_channels - 1 downto 0;
	subtype triplet is natural range num_triplets - 1 downto 0;
	
	function channel_to_triplet(ch : channel) return triplet is
	begin
		return (ch - channel'low) / channels_per_triplet + triplet'low;
	end function;
	
	signal tx_pll_clk : std_logic_vector(triplet);
	signal tx_pll_locked : std_logic_vector(triplet);	
	signal tx_pll_powerdown : std_logic_vector(triplet);

	-- TX PLL reconfiguration
	constant reconfig_from_pll_width : natural := 64;
	constant reconfig_to_pll_width : natural := 64;

	type pll_reconfig_from_pll is
		array(triplet) of
			std_logic_vector(reconfig_from_pll_width - 1 downto 0);
	type pll_reconfig_to_pll is
		array(triplet) of
			std_logic_vector(reconfig_to_pll_width - 1 downto 0);

	signal tx_pll_reconfig_from_pll : pll_reconfig_from_pll;
	signal tx_pll_reconfig_to_pll : pll_reconfig_to_pll;
	signal tx_pll_cal_busy : std_logic_vector(triplet);

	-- PHY reconfig controller interface
	constant reconfig_from_xcvr_width : natural := 46;
	constant reconfig_to_xcvr_width : natural := 70;

	signal reconfig_from_xcvr : std_logic_vector(num_channels * reconfig_from_xcvr_width - 1 downto 0);
	signal reconfig_to_xcvr : std_logic_vector(num_channels * reconfig_to_xcvr_width - 1 downto 0);
	signal reconfig_busy : std_logic;

	signal rx_analogreset : std_logic_vector(channel);
	signal rx_digitalreset : std_logic_vector(channel);
	signal rx_phy_cal_busy : std_logic_vector(channel);

	signal rx_set_locktoref : std_logic_vector(channel);
	signal rx_set_locktodata : std_logic_vector(channel);
	signal rx_std_signaldetect : std_logic_vector(channel);
	signal rx_is_lockedtoref : std_logic_vector(channel);
	signal rx_is_lockedtodata : std_logic_vector(channel);

	signal tx_phy_cal_busy : std_logic_vector(channel);
	signal tx_pll_or_phy_cal_busy : std_logic_vector(channel);
	signal tx_analogreset : std_logic_vector(channel);
	signal tx_analog_ready : std_logic_vector(channel);
	signal tx_digitalreset : std_logic_vector(channel);

	signal rx_ready : std_logic_vector(channel);
	signal tx_ready : std_logic_vector(channel);
begin
	tx_plls : for i in triplet generate
		-- TX PLL DPA output
		constant pll_dpa_output_width : natural := 8;
		signal dpa : std_logic_vector(pll_dpa_output_width - 1 downto 0);
	begin
		instance : entity sata_tx_pll.sata_tx_pll
			port map(
				refclk => ser_refclk(i),
				rst => tx_pll_powerdown(i),
				outclk_0 => open,
				-- for PLLs with reconfiguration enabled,
				-- the signal needs to be collected from
				-- the phase shifted outputs
				phout => dpa,
				locked => tx_pll_locked(i) --,
				-- reconfig_to_pll => tx_pll_reconfig_to_pll(i),
				-- reconfig_from_pll => tx_pll_reconfig_from_pll(i)
			);

		tx_pll_clk(i) <= dpa(0);

		tx_pll_reconfig_to_pll(i) <= (others => '0');

		-- temporary
		tx_pll_cal_busy(i) <= '0';		-- no reconfig controller connected
	end generate;

	channels : for i in channel generate
		-- split/merge the reconfig interface
		constant reconfig_from_xcvr_base : natural := reconfig_from_xcvr_width * i;
		constant reconfig_to_xcvr_base : natural := reconfig_to_xcvr_width * i;
		alias reconfig_from_xcvr_channel : std_logic_vector(reconfig_from_xcvr_width - 1 downto 0) is
			reconfig_from_xcvr(reconfig_from_xcvr_base + reconfig_from_xcvr_width - 1 downto reconfig_from_xcvr_base);
		alias reconfig_to_xcvr_channel : std_logic_vector(reconfig_to_xcvr_width - 1 downto 0) is
			reconfig_to_xcvr(reconfig_to_xcvr_base + reconfig_to_xcvr_width - 1 downto reconfig_to_xcvr_base);

		-- intermediate signal for data interface
		signal rx_parallel_data : std_logic_vector(63 downto 0);
		signal tx_parallel_data : std_logic_vector(43 downto 0);

	begin
		rx_data(i)(0).data <= rx_parallel_data(7 downto 0);
		rx_data(i)(0).datak <= rx_parallel_data(8);
		rx_data(i)(0).valid <= not rx_parallel_data(9);
		rx_data(i)(1).data <= rx_parallel_data(23 downto 16);
		rx_data(i)(1).datak <= rx_parallel_data(24);
		rx_data(i)(1).valid <= not rx_parallel_data(25);

		-- CycloneV Native PHY
		phy : entity sata_phy.sata_phy
			port map(
				-- reset
				rx_analogreset(0) => rx_analogreset(i),
				rx_digitalreset(0) => rx_digitalreset(i),
				tx_analogreset(0) => tx_analogreset(i),
				tx_digitalreset(0) => tx_digitalreset(i),

				-- clocks
				rx_cdr_refclk(0) => ser_refclk(channel_to_triplet(i)),
				ext_pll_clk(0) => tx_pll_clk(channel_to_triplet(i)),	-- TX clock from external PLL
				pll_powerdown(0) => '0',				-- no internal PLL, unused
				rx_std_coreclkin(0) => clk,
				tx_std_coreclkin(0) => clk,

				-- serial connection
				rx_serial_data(0) => ser_rx(i),
				tx_serial_data(0) => ser_tx(i),

				-- parallel connection
				rx_std_clkout(0) => rx_clk(i),
				rx_parallel_data => rx_parallel_data,

				tx_std_clkout(0) => tx_clk(i),
				tx_parallel_data => tx_parallel_data,

				-- rx_pma_clkout          : out std_logic_vector(0 downto 0);                     --          rx_pma_clkout.rx_pma_clkout

				rx_set_locktodata(0) => rx_set_locktodata(i),
				rx_set_locktoref(0) => rx_set_locktoref(i),
				rx_is_lockedtoref(0) => rx_is_lockedtoref(i),
				rx_is_lockedtodata(0) => rx_is_lockedtodata(i),

				rx_std_wa_patternalign(0) => '1',
				tx_std_elecidle(0) => tx_elecidle(i),
				rx_std_signaldetect(0) => rx_signaldetect(i),
				tx_cal_busy(0) => tx_phy_cal_busy(i),
				rx_cal_busy(0) => rx_phy_cal_busy(i),

				-- PHY reconfig interface
				reconfig_to_xcvr => reconfig_to_xcvr_channel,
				reconfig_from_xcvr => reconfig_from_xcvr_channel
			);
	end generate;

	phy_reconfig : entity sata_phy_reconfig.sata_phy_reconfig
		port map(
			mgmt_clk_clk => mgmt_clk_clk,
			mgmt_rst_reset => mgmt_rst_reset,

			reconfig_mgmt_address => reconfig_mgmt_address,
			reconfig_mgmt_read => reconfig_mgmt_read,
			reconfig_mgmt_readdata => reconfig_mgmt_readdata,
			reconfig_mgmt_waitrequest => reconfig_mgmt_waitrequest,
			reconfig_mgmt_write => reconfig_mgmt_write,
			reconfig_mgmt_writedata => reconfig_mgmt_writedata,

			reconfig_to_xcvr => reconfig_to_xcvr,
			reconfig_from_xcvr => reconfig_from_xcvr,
			reconfig_busy => reconfig_busy
		);

	phy_reset : entity work.sata_phy_reset_cyclonev
		generic map(
			num_rx_channels => 6,
			num_tx_plls => 2,
			num_tx_channels => 6
		)
		port map(
			reset => not nreset,
			clk => clk,

			rx_phy_cal_busy => rx_phy_cal_busy,
			rx_analogreset => rx_analogreset,
			rx_digitalreset => rx_digitalreset,

			rx_set_locktoref => rx_set_locktoref,
			rx_set_locktodata => rx_set_locktodata,
			rx_std_signaldetect => rx_std_signaldetect,
			rx_is_lockedtoref => rx_is_lockedtoref,
			rx_is_lockedtodata => rx_is_lockedtodata,

			tx_pll_cal_busy => tx_pll_cal_busy,
			tx_pll_powerdown => tx_pll_powerdown,
			tx_pll_locked => tx_pll_locked,

			tx_phy_cal_busy => tx_phy_cal_busy,
			tx_analogreset => tx_analogreset,
			tx_digitalreset => tx_digitalreset,

			rx_ready => rx_ready,
			tx_ready => tx_ready
		);
end architecture;
