#! /bin/sh -x

set -e

if [ "$#" = 0 ]; then
	testbenches="`ls tb_*.vhdl`"
else
	testbenches="$*"
fi

. ./quartus-env.sh

cd simulation

# compile design
for i in p b e a c; do
	vcom -2008 -just $i ../*.vhdl
done

LIBRARIES="`grep 'eval vsim' mentor/msim_setup.tcl | grep -oe '-L \w*' | awk '!seen[$0] {print} {++seen[$0]}'`"

enable_tbbmalloc_workaround

for i in ${testbenches}; do
	tb=`basename $i .vhdl`
	vsim -l $tb.log -t ps -c ${LIBRARIES} $tb -do "vcd file $tb.vcd" -do "vcd add -r *" -do "run -a" -do "quit" && vcd2fst $tb.vcd $tb.fst &
done

wait

disable_tbbmalloc_workaround
