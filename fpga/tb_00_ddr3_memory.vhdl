library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

library std;
use std.env.finish;

entity tb_00_ddr3_memory is
end entity;

architecture sim of tb_00_ddr3_memory is
	signal mem_a : std_logic_vector(15 downto 0) := (others => 'U');
	signal mem_ba : std_logic_vector(2 downto 0) := (others => 'U');
	signal mem_ck, mem_ck_n : std_logic := '0';
	signal mem_cke : std_logic := 'U';
	signal mem_cs_n : std_logic := 'U';
	signal mem_dm : std_logic := 'U';
	signal mem_odt : std_logic := 'U';
	signal mem_ras_n, mem_cas_n : std_logic := 'U';
	signal mem_we_n : std_logic := 'U';
	signal mem_reset_n : std_logic := 'U';
	signal mem_dq : std_logic_vector(7 downto 0) := (others => 'U');
	signal mem_dqs, mem_dqs_n : std_logic := 'U';

	attribute period : time;
	attribute period of mem_ck : signal is 2 ns;	-- 500 MHz
begin
	-- sim timeout
	process
	begin
		wait for 10 us;
		finish;
	end process;

	-- stimuli
	mem_ck <= not mem_ck after mem_ck'period / 2;
	mem_ck_n <= not mem_ck;

	-- not yet implemented
	mem_dm <= '0';
	mem_odt <= '0';

	process
		procedure set_control(
			constant cke : in std_logic := '1';
			constant cs_n : in std_logic := '1';
			constant ras_n : in std_logic := '1';
			constant cas_n : in std_logic := '1';
			constant we_n : in std_logic := '1';
			constant ba : in std_logic_vector(2 downto 0) := (others => 'U');
			constant a : in std_logic_vector(15 downto 0) := (others => 'U')) is
		begin
			-- delay by a quarter cycle
			mem_cke <= transport cke after mem_ck'period / 4;
			mem_cs_n <= transport cs_n after mem_ck'period / 4;
			mem_ras_n <= transport ras_n after mem_ck'period / 4;
			mem_cas_n <= transport cas_n after mem_ck'period / 4;
			mem_we_n <= transport we_n after mem_ck'period / 4;
			mem_ba <= transport ba after mem_ck'period / 4;
			mem_a <= transport a after mem_ck'period / 4;
		end procedure;

		procedure set_data(
			constant dqs : in std_logic := 'Z';
			constant dq : in std_logic_vector(7 downto 0) := (others => 'Z')) is
		begin
			mem_dq <= transport dq after mem_ck'period / 4;
			mem_dqs <= transport dqs after mem_ck'period / 2;
			case dqs is 	-- not the not operator :/
				 when '1' => mem_dqs_n <= transport '0' after mem_ck'period / 2;
				 when '0' => mem_dqs_n <= transport '1' after mem_ck'period / 2;
				 when 'Z' => mem_dqs_n <= transport 'Z' after mem_ck'period / 2;
				 when 'H' => mem_dqs_n <= transport 'L' after mem_ck'period / 2;
				 when 'L' => mem_dqs_n <= transport 'H' after mem_ck'period / 2;
				 when 'W' => mem_dqs_n <= transport 'W' after mem_ck'period / 2;
				 when 'U' => mem_dqs_n <= transport 'U' after mem_ck'period / 2;
				 when 'X' => mem_dqs_n <= transport 'X' after mem_ck'period / 2;
				 when '-' => mem_dqs_n <= transport '-' after mem_ck'period / 2;
			 end case;
		end procedure;
	begin
		-- reset
		mem_reset_n <= '0';
		set_control(cke => 'U', cs_n => 'U', ras_n => 'U', cas_n => 'U', we_n => 'U');
		wait for 50 ns;
		-- prepare end of reset
		set_control(cke => '0');	-- come out of reset with CKE disabled
		set_data;
		wait for 10 ns;
		mem_reset_n <= '1';
		wait for 10 ns;

		-- come out of powersave
		wait until falling_edge(mem_ck);
		set_control(cs_n => '1');

		-- set mode registers
		-- TODO: timing constraints
		wait until falling_edge(mem_ck);
		set_control(cs_n => '0', ras_n => '0', cas_n => '0', we_n => '0', ba => "010", a => "UUU0000000001000");
		wait until falling_edge(mem_ck);
		set_control(cs_n => '0', ras_n => '0', cas_n => '0', we_n => '0', ba => "011", a => "UUU0000000000000");
		wait until falling_edge(mem_ck);
		set_control(cs_n => '0', ras_n => '0', cas_n => '0', we_n => '0', ba => "001", a => "UUU0000000000000");
		wait until falling_edge(mem_ck);
		set_control(cs_n => '0', ras_n => '0', cas_n => '0', we_n => '0', ba => "000", a => "UUU0101100110000");
		wait until falling_edge(mem_ck);
		set_control;
		wait until falling_edge(mem_ck);
		
		-- activate row
		-- TODO: timing
		wait until falling_edge(mem_ck);
		set_control(cs_n => '0', ras_n => '0', ba => "000", a => "0000000000000000");
		wait until falling_edge(mem_ck);
		set_control;
		wait until falling_edge(mem_ck);

		-- start a write burst
		-- TODO: timing
		wait until falling_edge(mem_ck);
		set_control(cs_n => '0', cas_n => '0', we_n => '0', ba => "000", a => "UUUUU00000000000");

		-- CWL = 6 cycles

		wait until falling_edge(mem_ck);
		set_control;

		for i in 1 to 5 loop
			wait until rising_edge(mem_ck);
		end loop;

		set_data(dqs => mem_ck);
		wait until mem_ck'event;
		set_data(dqs => mem_ck);
		wait until mem_ck'event;

		for i in 0 to 7 loop
			set_data(dqs => mem_ck, dq => std_logic_vector(to_unsigned(i, 8)));
			wait until mem_ck'event;
		end loop;

		set_data;

		wait until falling_edge(mem_ck);
		-- start a read burst
		-- TODO: timing
		set_control(cs_n => '0', cas_n => '0', we_n => '1', ba => "000", a => "UUUUU00000000000");
		wait until falling_edge(mem_ck);
		set_control;

		wait;
	end process;

	dut : entity work.ddr3_memory
		generic map(
			col_width => 10,
			row_width => 16,
			bank_width => 3,
			word_width => 8
		)
		port map(
			a => mem_a,
			ba => mem_ba,
			ck => mem_ck,
			ck_n => mem_ck_n,
			cke => mem_cke,
			cs_n => mem_cs_n,
			dm => mem_dm,
			odt => mem_odt,
			ras_n => mem_ras_n,
			cas_n => mem_cas_n,
			we_n => mem_we_n,
			reset_n => mem_reset_n,

			-- bidirectional signals are split
			dq_i => mem_dq,
			dqs_i => mem_dqs,
			dqs_n_i => mem_dqs_n,
			dq_o => mem_dq,
			dqs_o => mem_dqs,
			dqs_n_o => mem_dqs_n
		);
end architecture;
