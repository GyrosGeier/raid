library ieee;
use ieee.std_logic_1164.ALL;

library std;
use std.env.finish;

library ddr3_bottom;
use ddr3_bottom.ddr3_bottom;

entity tb_10_mem_startup is
end entity;

architecture sim of tb_10_mem_startup is
	-- memory layout
	constant rank_count : natural := 2;
	constant dqs_group_width : natural := 8;
	constant dqs_group_count : natural := 5;

	-- TODO: instantiate all chips
	subtype rank is natural range 0 to rank_count - 1;
	subtype dqs_group is natural range 0 to dqs_group_count - 1;

	signal pll_ref_clk                : std_logic := '0';
	signal global_reset_n             : std_logic;
	signal soft_reset_n               : std_logic;
	signal afi_clk                    : std_logic;
	signal afi_half_clk               : std_logic;
	signal afi_reset_n                : std_logic;
	signal afi_reset_export_n         : std_logic;
	signal mem_a                      : std_logic_vector(15 downto 0);
	signal mem_ba                     : std_logic_vector(2 downto 0);
	signal mem_ck                     : std_logic_vector(0 downto 0);
	signal mem_ck_n                   : std_logic_vector(0 downto 0);
	signal mem_cke                    : std_logic_vector(1 downto 0);
	signal mem_cs_n                   : std_logic_vector(1 downto 0);
	signal mem_dm                     : std_logic_vector(4 downto 0);
	signal mem_ras_n                  : std_logic;
	signal mem_cas_n                  : std_logic;
	signal mem_we_n                   : std_logic;
	signal mem_reset_n                : std_logic;
	signal mem_dq                     : std_logic_vector(39 downto 0);
	signal mem_dqs                    : std_logic_vector(4 downto 0);
	signal mem_dqs_n                  : std_logic_vector(4 downto 0);
	signal mem_odt                    : std_logic_vector(1 downto 0);
	signal avl_ready_0                : std_logic;
	signal avl_burstbegin_0           : std_logic;
	signal avl_addr_0                 : std_logic_vector(29 downto 0);
	signal avl_rdata_valid_0          : std_logic;
	signal avl_rdata_0                : std_logic_vector(31 downto 0);
	signal avl_wdata_0                : std_logic_vector(31 downto 0);
	signal avl_be_0                   : std_logic_vector(3 downto 0);
	signal avl_read_req_0             : std_logic;
	signal avl_write_req_0            : std_logic;
	signal avl_size_0                 : std_logic_vector(2 downto 0);
	signal mp_cmd_clk_0_clk           : std_logic;
	signal mp_cmd_reset_n_0_reset_n   : std_logic;
	signal mp_rfifo_clk_0_clk         : std_logic;
	signal mp_rfifo_reset_n_0_reset_n : std_logic;
	signal mp_wfifo_clk_0_clk         : std_logic;
	signal mp_wfifo_reset_n_0_reset_n : std_logic;

	signal csr_clk                    : std_logic := '0';
	signal csr_reset_n                : std_logic;

	signal local_init_done            : std_logic;
	signal local_cal_success          : std_logic;
	signal local_cal_fail             : std_logic;
	signal oct_rzqin                  : std_logic;
	signal pll_mem_clk                : std_logic;
	signal pll_write_clk              : std_logic;
	signal pll_locked                 : std_logic;
	signal pll_write_clk_pre_phy_clk  : std_logic;
	signal pll_addr_cmd_clk           : std_logic;
	signal pll_avl_clk                : std_logic;
	signal pll_config_clk             : std_logic;
	signal pll_mem_phy_clk            : std_logic;
	signal afi_phy_clk                : std_logic;
	signal pll_avl_phy_clk            : std_logic;
	signal csr_addr                   : std_logic_vector(15 downto 0);
	signal csr_read_req               : std_logic;
	signal csr_rdata                  : std_logic_vector(31 downto 0);
	signal csr_write_req              : std_logic;
	signal csr_wdata                  : std_logic_vector(31 downto 0);
	signal csr_waitrequest            : std_logic;
	signal csr_be                     : std_logic_vector(3 downto 0);
	signal csr_rdata_valid            : std_logic;
begin
	-- sim timeout
	process
	begin
		wait for 500 us;
		finish;
	end process;

	-- DDR3 controller clock/reset
	pll_ref_clk <= not pll_ref_clk after 1 ns;	-- 500 MHz
	global_reset_n <= 'U', '0' after 5 ns, '1' after 50 ns;
	soft_reset_n <= 'U', '0' after 5 ns, '1' after 70 ns;

	-- Avalon-MM interface for control signals
	csr_clk <= not csr_clk after 5 ns;		-- 100 MHz
	csr_reset_n <= 'U', '0' after 4 ns, '1' after 90 ns;

	csr_addr <= (others => '0');
	csr_be <= (others => '0');
	csr_read_req <= '0';
	csr_write_req <= '0';
	csr_wdata <= (others => 'U');

	-- Avalon-MM interface for data
	avl_addr_0 <= (others => '0');
	avl_burstbegin_0 <= '0';
	avl_be_0 <= (others => '0');
	avl_read_req_0 <= '0';
	avl_write_req_0 <= '0';
	avl_wdata_0 <= (others => 'U');

	dut : entity ddr3_bottom.ddr3_bottom
		port map(
			pll_ref_clk => pll_ref_clk,
			global_reset_n => global_reset_n,
			soft_reset_n => soft_reset_n,
			afi_clk => afi_clk,
			afi_half_clk => afi_half_clk,
			afi_reset_n => afi_reset_n,
			afi_reset_export_n => afi_reset_export_n,
			mem_a => mem_a,
			mem_ba => mem_ba,
			mem_ck => mem_ck,
			mem_ck_n => mem_ck_n,
			mem_cke => mem_cke,
			mem_cs_n => mem_cs_n,
			mem_dm => mem_dm,
			mem_ras_n(0) => mem_ras_n,
			mem_cas_n(0) => mem_cas_n,
			mem_we_n(0) => mem_we_n,
			mem_reset_n => mem_reset_n,
			mem_dq => mem_dq,
			mem_dqs => mem_dqs,
			mem_dqs_n => mem_dqs_n,
			mem_odt => mem_odt,
			avl_ready_0 => avl_ready_0,
			avl_burstbegin_0 => avl_burstbegin_0,
			avl_addr_0 => avl_addr_0,
			avl_rdata_valid_0 => avl_rdata_valid_0,
			avl_rdata_0 => avl_rdata_0,
			avl_wdata_0 => avl_wdata_0,
			avl_be_0 => avl_be_0,
			avl_read_req_0 => avl_read_req_0,
			avl_write_req_0 => avl_write_req_0,
			avl_size_0 => avl_size_0,
			mp_cmd_clk_0_clk => mp_cmd_clk_0_clk,
			mp_cmd_reset_n_0_reset_n => mp_cmd_reset_n_0_reset_n,
			mp_rfifo_clk_0_clk => mp_rfifo_clk_0_clk,
			mp_rfifo_reset_n_0_reset_n => mp_rfifo_reset_n_0_reset_n,
			mp_wfifo_clk_0_clk => mp_wfifo_clk_0_clk,
			mp_wfifo_reset_n_0_reset_n => mp_wfifo_reset_n_0_reset_n,
			csr_clk => csr_clk,
			csr_reset_n => csr_reset_n,
			local_init_done => local_init_done,
			local_cal_success => local_cal_success,
			local_cal_fail => local_cal_fail,
			oct_rzqin => oct_rzqin,
			pll_mem_clk => pll_mem_clk,
			pll_write_clk => pll_write_clk,
			pll_locked => pll_locked,
			pll_write_clk_pre_phy_clk => pll_write_clk_pre_phy_clk,
			pll_addr_cmd_clk => pll_addr_cmd_clk,
			pll_avl_clk => pll_avl_clk,
			pll_config_clk => pll_config_clk,
			pll_mem_phy_clk => pll_mem_phy_clk,
			afi_phy_clk => afi_phy_clk,
			pll_avl_phy_clk => pll_avl_phy_clk,
			csr_addr => csr_addr,
			csr_read_req => csr_read_req,
			csr_rdata => csr_rdata,
			csr_write_req => csr_write_req,
			csr_wdata => csr_wdata,
			csr_waitrequest => csr_waitrequest,
			csr_be => csr_be,
			csr_rdata_valid => csr_rdata_valid
		);

	ranks : for r in rank generate
	begin
		groups : for g in dqs_group generate
			-- reset is common
			alias reset_n : std_logic is mem_reset_n;

			-- clock is common (but could be per-rank)
			alias ck : std_logic is mem_ck(0);
			alias ck_n : std_logic is mem_ck_n(0);

			-- clock enable, chip select and termination control are per-rank
			alias cke : std_logic is mem_cke(r);
			alias cs_n : std_logic is mem_cs_n(r);
			alias odt : std_logic is mem_odt(r);

			-- other flyby bus signals are common
			alias a : std_logic_vector(15 downto 0) is mem_a;
			alias ba : std_logic_vector(2 downto 0) is mem_ba;
			alias ras_n : std_logic is mem_ras_n;
			alias cas_n : std_logic is mem_cas_n;
			alias we_n : std_logic is mem_we_n;

			-- DQ/DQS/DM are per-DQS-group
			alias dq_i : std_logic_vector(dqs_group_width - 1 downto 0) is
				mem_dq(g * dqs_group_width + dqs_group_width - 1 downto g * dqs_group_width);
			alias dqs_i : std_logic is mem_dqs(g);
			alias dqs_n_i : std_logic is mem_dqs_n(g);

			alias dq_o : std_logic_vector(dqs_group_width - 1 downto 0) is
				mem_dq(g * dqs_group_width + dqs_group_width - 1 downto g * dqs_group_width);
			alias dqs_o : std_logic is mem_dqs(g);
			alias dqs_n_o : std_logic is mem_dqs_n(g);

			alias dm : std_logic is mem_dm(g);
		begin
			mem : entity work.ddr3_memory
				generic map(
					col_width => 10,
					row_width => 16,
					bank_width => 3,
					word_width => 8
				)
				port map(
					a => a,
					ba => ba,
					ck => ck,
					ck_n => ck_n,
					cke => cke,
					cs_n => cs_n,
					dm => dm,
					odt => odt,
					ras_n => ras_n,
					cas_n => cas_n,
					we_n => we_n,
					reset_n => reset_n,

					dq_i => dq_i,
					dqs_i => dqs_i,
					dqs_n_i => dqs_n_i,
					dq_o => dq_o,
					dqs_o => dqs_o,
					dqs_n_o => dqs_n_o
				);
		end generate;
	end generate;
end architecture;
