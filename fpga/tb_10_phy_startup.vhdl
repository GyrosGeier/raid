library IEEE;
use IEEE.std_logic_1164.ALL;

use std.env.finish;

library sata_phy;
use sata_phy.sata_phy;

entity tb_10_phy_startup is
end entity;

architecture rtl of tb_10_phy_startup is
	signal reset : std_logic;
	signal clk : std_logic;

	signal reconfig_reset : std_logic;
	signal reconfig_clk : std_logic;

	signal tx_pll_cal_busy : std_logic;
	signal tx_pll_powerdown : std_logic;
	signal tx_pll_locked : std_logic;

	signal pll_powerdown          : std_logic;
	signal tx_analogreset         : std_logic;
	signal tx_digitalreset        : std_logic;
	signal tx_serial_data         : std_logic;
	signal ext_pll_clk            : std_logic;
	signal rx_analogreset         : std_logic;
	signal rx_digitalreset        : std_logic;
	signal rx_cdr_refclk          : std_logic;
	signal rx_pma_clkout          : std_logic;
	signal rx_serial_data         : std_logic;
	signal rx_set_locktodata      : std_logic;
	signal rx_set_locktoref       : std_logic;
	signal rx_is_lockedtoref      : std_logic;
	signal rx_is_lockedtodata     : std_logic;
	signal tx_parallel_data       : std_logic_vector(43 downto 0);
	signal rx_parallel_data       : std_logic_vector(63 downto 0);
	signal tx_std_coreclkin       : std_logic;
	signal rx_std_coreclkin       : std_logic;
	signal tx_std_clkout          : std_logic;
	signal rx_std_clkout          : std_logic;
	signal rx_std_wa_patternalign : std_logic;
	signal tx_std_elecidle        : std_logic;
	signal rx_std_signaldetect    : std_logic;
	signal tx_phy_cal_busy        : std_logic;
	signal rx_phy_cal_busy        : std_logic;
	signal reconfig_to_xcvr       : std_logic_vector(69 downto 0);
	signal reconfig_from_xcvr     : std_logic_vector(45 downto 0);

	signal rx_ready : std_logic;
	signal tx_ready : std_logic;
begin
	-- simulation ends when transceiver is ready
	process
	begin
		wait until rx_ready = '1' and tx_ready = '1';
		wait for 10 us;
		finish;
	end process;

	-- simulation timeout
	process
	begin
		wait for 40 us;
		finish;
	end process;

	-- simulation reset
	reset <= '1', '0' after 50 ns;

	-- 125 MHz logic clock
	clk <= '0' when reset = '1' else
	       not clk after 4 ns;

	-- 500 MHz CDR refclk
	rx_cdr_refclk <= '0' when reset = '1' else
			 not rx_cdr_refclk after 1 ns;

	-- 1500 MHz TX clock
	-- needs trickery because the period is not a multiple of 1 ps
	-- so this is derived from the CDR refclk, with extra transitions
	-- inserted
	gen_ext_pll_clk : process
	begin
		wait until rx_cdr_refclk'event;
		ext_pll_clk <= rx_cdr_refclk;
		wait for 333 ps;
		ext_pll_clk <= not rx_cdr_refclk;
		wait for 334 ps;
		ext_pll_clk <= rx_cdr_refclk;
	end process;

	tx_pll_cal_busy <= '0';
	tx_pll_locked <= '0' when tx_pll_powerdown else
			 '1' after 10 us;

	-- PLL powerdown is unused (TX PLL is outside the PHY IP)
	pll_powerdown <= '0';

	reconfig_clk <= clk;
	reconfig_reset <= '1', '0' after 100 ns;

	-- reconfig doesn't do anything, but provide clk/reset to reconfig hard IP
	reconfig_to_xcvr <= (0 => reconfig_clk, 1 => reconfig_reset, others => '0');

	phy : entity sata_phy.sata_phy
		port map(
			pll_powerdown(0) => pll_powerdown,
			tx_analogreset(0) => tx_analogreset,
			tx_digitalreset(0) => tx_digitalreset,
			tx_serial_data(0) => tx_serial_data,
			ext_pll_clk(0) => ext_pll_clk,
			rx_analogreset(0) => rx_analogreset,
			rx_digitalreset(0) => rx_digitalreset,
			rx_cdr_refclk(0) => rx_cdr_refclk,
			rx_pma_clkout(0) => rx_pma_clkout,
			rx_serial_data(0) => rx_serial_data,
			rx_set_locktodata(0) => rx_set_locktodata,
			rx_set_locktoref(0) => rx_set_locktoref,
			rx_is_lockedtoref(0) => rx_is_lockedtoref,
			rx_is_lockedtodata(0) => rx_is_lockedtodata,
			tx_parallel_data => tx_parallel_data,
			rx_parallel_data => rx_parallel_data,
			tx_std_coreclkin(0) => tx_std_coreclkin,
			rx_std_coreclkin(0) => rx_std_coreclkin,
			tx_std_clkout(0) => tx_std_clkout,
			rx_std_clkout(0) => rx_std_clkout,
			rx_std_wa_patternalign(0) => rx_std_wa_patternalign,
			tx_std_elecidle(0) => tx_std_elecidle,
			rx_std_signaldetect(0) => rx_std_signaldetect,
			tx_cal_busy(0) => tx_phy_cal_busy,
			rx_cal_busy(0) => rx_phy_cal_busy,
			reconfig_to_xcvr => reconfig_to_xcvr,
			reconfig_from_xcvr => reconfig_from_xcvr
		);

	reset_controller : entity work.sata_phy_reset_cyclonev
		generic map(
			num_tx_plls => 1,
			num_tx_channels => 1,
			num_rx_channels => 1
		)
		port map(
			reset => reset,
			clk => clk,

			rx_phy_cal_busy(0) => rx_phy_cal_busy,
			rx_analogreset(0) => rx_analogreset,
			rx_digitalreset(0) => rx_digitalreset,

			rx_set_locktoref(0) => rx_set_locktoref,
			rx_set_locktodata(0) => rx_set_locktodata,
			rx_std_signaldetect(0) => rx_std_signaldetect,
			rx_is_lockedtoref(0) => rx_is_lockedtoref,
			rx_is_lockedtodata(0) => rx_is_lockedtodata,

			tx_pll_cal_busy(0) => tx_pll_cal_busy,
			tx_pll_powerdown(0) => tx_pll_powerdown,
			tx_pll_locked(0) => tx_pll_locked,

			tx_phy_cal_busy(0) => tx_phy_cal_busy,
			tx_analogreset(0) => tx_analogreset,
			tx_digitalreset(0) => tx_digitalreset,

			rx_ready(0) => rx_ready,
			tx_ready(0) => tx_ready
		);

	rx_std_wa_patternalign <= '0';

	rx_std_coreclkin <= clk;
	tx_std_coreclkin <= clk;

	-- test TX data
	tx_parallel_data <= "00" & "1" & "10111100" &		-- K28.5, no forced disparity
			    "00" & "1" & "10111100" &
			    "00" & "1" & "10111100" &
			    "00" & "1" & "10111100";
	tx_std_elecidle <= '0';

	-- test RX data
	process
	begin
		rx_serial_data <= '0';
		wait for 1333 ps;
		rx_serial_data <= '1';
		wait for 3333 ps;
		rx_serial_data <= '0';
		wait for 666 ps;
		rx_serial_data <= '1';
		wait for 666 ps;
		rx_serial_data <= '0';
		wait for 666 ps;
		rx_serial_data <= '1';
		wait for 1333 ps;
		rx_serial_data <= '0';
		wait for 3333 ps;
		rx_serial_data <= '1';
		wait for 666 ps;
		rx_serial_data <= '0';
		wait for 666 ps;
		rx_serial_data <= '1';
		wait for 666 ps;
	end process;
end architecture;
