library IEEE;
use IEEE.std_logic_1164.ALL;

library sata_tx_pll;
use sata_tx_pll.sata_tx_pll;

use std.env.finish;

entity tb_10_tx_pll_startup is
end entity;

architecture sim of tb_10_tx_pll_startup is
	signal nreset : std_logic;
	signal refclk : std_logic;
	signal outclk : std_logic;
	signal locked : std_logic;

	signal dpa : std_logic_vector(7 downto 0);
begin
	-- unit under test
	uut: entity sata_tx_pll.sata_tx_pll
		port map(
			refclk => refclk,
			rst => not nreset,
			outclk_0 => open,	-- ignore clock from divider
			locked => locked,
			phout => dpa
		);

	outclk <= dpa(0);			-- use clock from phase out

	-- stimuli
	nreset <= '0', '1' after 50 ns;
	refclk <= '0' when nreset = '0' else
		  not refclk after 1 ns;	-- 500 MHz

	-- tests
	process
		variable starttime : time;
	begin
		wait until nreset = '1';
		starttime := now;
		wait until locked = '1';
		report time'image(now - starttime);
		finish;
	end process;

	-- timeout
	process
	begin
		wait for 10 us;
		assert false report "timeout" severity failure;
	end process;
end architecture;
