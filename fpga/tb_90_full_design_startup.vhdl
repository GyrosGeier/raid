library ieee;

use ieee.std_logic_1164.ALL;

use std.env.finish;

entity tb_90_full_design_startup is
end entity;

architecture sim of tb_90_full_design_startup is
	signal clkreset : std_logic;
	signal nreset : std_logic;
	signal sata_refclk : std_logic_vector(1 downto 0);
	signal mem32_refclk : std_logic;

	signal gen1_clk : std_logic;

	signal idle : std_logic;
begin
	process
	begin
		wait for 200 us;
		finish;
	end process;

	clkreset <= '0', '1' after 5 ns;
	nreset <= '0', '1' after 30 ns;

	sata_refclk(0) <= '0' when clkreset = '0' else
			not sata_refclk(0) after 1 ns;	-- 500 MHz
	sata_refclk(1) <= '0' when clkreset = '0' else
			sata_refclk(0) after 250 ps;
	mem32_refclk <= '0' when clkreset = '0' else
			sata_refclk(0) after 500 ps;

	-- 1.5 GHz clock that doesn't phase shift against the sata_refclk
	process
	begin
		gen1_clk <= '1';
		wait for 333 ps;
		gen1_clk <= '0';
		wait for 333 ps;
		gen1_clk <= '1';
		wait for 334 ps;
		gen1_clk <= '0';
		wait for 333 ps;
		gen1_clk <= '1';
		wait for 333 ps;
		gen1_clk <= '0';
		wait for 334 ps;
	end process;

	dut : entity work.raid
		port map(
			nreset => nreset,

			sata_refclk => sata_refclk,
			sata_rx => (others => idle),
			sata_tx => open,

			mem32_refclk => mem32_refclk,
			mem32_a => open,
			mem32_ba => open,
			mem32_ck => open,
			mem32_ck_n => open,
			mem32_cke => open,
			mem32_cs_n => open,
			mem32_dm => open,
			mem32_ras_n => open,
			mem32_cas_n => open,
			mem32_we_n => open,
			mem32_reset_n => open,
			mem32_dq => open,
			mem32_dqs => open,
			mem32_dqs_n => open,
			mem32_odt => open,
		
			mem32_oct_rzqin => '0',

			eth_rx_clk_0          => '0',
			eth_rgmii_in_0        => "0000",
			eth_rx_control_0      => '0',
			eth_tx_clk_0          => open,
			eth_rgmii_out_0       => open,
			eth_tx_control_0      => open,

			eth_rx_clk_1          => '0',
			eth_rgmii_in_1        => "0000",
			eth_rx_control_1      => '0',
			eth_tx_clk_1          => open,
			eth_rgmii_out_1       => open,
			eth_tx_control_1      => open,

			eth_rx_clk_2          => '0',
			eth_rgmii_in_2        => "0000",
			eth_rx_control_2      => '0',
			eth_tx_clk_2          => open,
			eth_rgmii_out_2       => open,
			eth_tx_control_2      => open,

			eth_rx_clk_3          => '0',
			eth_rgmii_in_3        => "0000",
			eth_rx_control_3      => '0',
			eth_tx_clk_3          => open,
			eth_rgmii_out_3       => open,
			eth_tx_control_3      => open,

			eth_mdc => open,
			eth_mdio => open
		);

	process
	begin
		idle <= '0';
		wait until rising_edge(gen1_clk);
		wait until rising_edge(gen1_clk);
		idle <= '1';
		wait until rising_edge(gen1_clk);
		wait until rising_edge(gen1_clk);
		wait until rising_edge(gen1_clk);
		wait until rising_edge(gen1_clk);
		wait until rising_edge(gen1_clk);
		idle <= '0';
		wait until rising_edge(gen1_clk);
		idle <= '1';
		wait until rising_edge(gen1_clk);
		idle <= '0';
		wait until rising_edge(gen1_clk);
		idle <= '1';
		wait until rising_edge(gen1_clk);
		wait until rising_edge(gen1_clk);
		idle <= '0';
		wait until rising_edge(gen1_clk);
		wait until rising_edge(gen1_clk);
		wait until rising_edge(gen1_clk);
		wait until rising_edge(gen1_clk);
		wait until rising_edge(gen1_clk);
		idle <= '1';
		wait until rising_edge(gen1_clk);
		idle <= '0';
		wait until rising_edge(gen1_clk);
		idle <= '1';
		wait until rising_edge(gen1_clk);
	end process;
end architecture;
